jQuery.fn.extend({
    animateCss(animationName) {
        const animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass(`animated ${animationName} show`).one(animationEnd, function() {
            $(this).removeClass(`animated ${animationName}`);
        });
    },
    bgCss() {
        const imgData = $(this).data('background');
        $(this).each(() => {
            // $(this).css('background-image', 'url(' + imgData + ')');
        });
    }
});

//Method DOM
const base = {
	initialize() {
		this.events();
	},
	
	events() {
		//Step
        jQuery(".js-pyStepz").steps({
			headerTag: ".js-pyStepz__title",
			bodyTag: ".py-content",
			transitionEffect: "slideLeft"
		});
    }
};


$(window).on('load', function () { 
	base.initialize();
});